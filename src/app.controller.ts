import { Controller, Get, HttpException } from '@nestjs/common';

@Controller()
export class AppController {
  async causeErrorAfterDelay(err, delay = 0) {
    await new Promise( resolve => setTimeout(resolve, delay) )
    throw new HttpException(err, 400)
  }

  @Get('1')
  async throwErrorInAwaitedPromise(): Promise<string> {
    await this.causeErrorAfterDelay("doesnt cause crash")
    await new Promise( resolve => setTimeout(resolve, 500) )
    return 'responseReturned'
  }

  @Get('2')
  async throwErrorInPendingPromise(): Promise<string> {
    const err = this.causeErrorAfterDelay("crashes in pending promise", 250)
    await new Promise( resolve => setTimeout(resolve, 500) )
    await err
    return 'responseReturned'
  }

  @Get('3')
  async throwErrorInCompletePromise(): Promise<string> {
    const err = this.causeErrorAfterDelay("doesnt cause crash as awaited before", 500)
    await new Promise( resolve => setTimeout(resolve, 250) )
    await err
    return 'responseReturned'
  }

  @Get('4')
  async throwErrorAfterResponseReturned(): Promise<string> {
    this.causeErrorAfterDelay("returns response but crashes app afterward", 500)
    return 'responseReturned'
  }
}
